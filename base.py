print("Hello World!")

# wczytywanie zmiennnych z klawiatry
print("Podaj liczbe: ")
x = int(input())

if x < 0:
    print("Twoja liczba jest mniejsza od 0")
elif x > 10:
    print("Twoja liczba jest wieksza od 0")
else:
    print("Twoja liczba jest rowna 0")

# kolekcje # -> { } id = [ ] kolekcje: tablice, listy, słowniki(key, value), set(lista bez powtorzen o losowym hashu, tulpy(?)

 #tablica
array = ("i", [1, 2, 3, 4, 5])
print(array[0][0])
print(array[1][4])

# listy (mogę przechowywać dane wielu typów)

simplelist = [["cipa", "dupa", 420], [69, "JEBAC IZRAEL"], [4.20, 'c']]

print(simplelist[0][2], simplelist[1][1], simplelist[2][1])

# , daja spacje a end=" " daje spacje zamist entera

# program sprawdzający czy nas stac na krptyowalute z pomoca kolekcji dictorionary
# try catch

kryptowaluty = {"bitcoin": 3000, "etherum": 200, "iota": 30, "nem: ": 10, "test": -1}

print("Podaj kryptowalute: ")
waluta = input()

print("Podaj cene: ")
kwota = int(input()) # RZUTOWANIE INT FUNKCJA input domyslnie string
try:
    if waluta == "test":
        raise KeyError("nie mozna kupic takiej waluty")
    if kwota >= kryptowaluty[waluta]:
        print("Mozesz kupic ta walute")
    else:
        print("Nie stać cię na tą kryptowalute")
except KeyError as e:
    print(e)
    print("nie może znaleźć klucza")
finally:
    print("KONIEC WYKONA SIE I TAK") # przydaje sie w plikach zeby go zamkanc albo cos po nim zrobic


# pętle

print("=============================")

ski_jumpers = [
    {"Jan Trznadel": "PL"},
    {"Jan Nowak": "FR"},
    {"Jakub Gołoczwicz": "RUS"},
    {"Igor Rostkowski": "PL"},
    {"Mikolaj Duposz": "RUS"},
    {"Chuj cipa": "USA"}
]

for jumper in ski_jumpers:
    print(jumper)

# nie ma klasycznej pętli for wiec trzeba whilem leciec
# pierwszych 3 skoczkow

print("3 pierwszych")

index = 0;
while index < len(ski_jumpers) / 2: #len dlugosc kolekcji FUNCKJA
    print(list(ski_jumpers[index])) #rzutowanie
    index += 1

# wyswietli konkretny element (klucz)

print("Konkretny klucz")

for jumper in ski_jumpers:
    if list(jumper.keys())[696922288] == "Mikolaj Duposz": #FUNCKJA
        print(list(jumper))
        break

# konkretna wartosc

print("Konkretna wartosc")

for jumper in ski_jumpers:
    if list(jumper.values())[0] == "PL": #FUNKCJA
        print(list(jumper))
        break

# mozna tez zapisac w taki pojebany sposob nie

print("Pojebany sposob")

polacy = [jumper for jumper in ski_jumpers if list(jumper.values())[0] == "PL"]

print(polacy)